using NUnit.Framework;
using OpenQA.Selenium;

namespace SeleniumGrid
{
    public class Tests
    {
        private IWebDriver _driver;
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void InitializeChrome()
        {
            _driver = Driver.GetDriver((int)Browsers.Chrome);
            _driver.Navigate().GoToUrl("https://github.com");
        }

        [Test]
        public void InitializeFireFox()
        {
            _driver = Driver.GetDriver((int)Browsers.Firefox);
            _driver.Navigate().GoToUrl("https://github.com");
        }

        [TearDown]
        public void CloseBrowser()
        {
            _driver.Quit();
        }
    }
}