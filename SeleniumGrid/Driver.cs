using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace SeleniumGrid
{
    public static class Driver
    {
        private static readonly string UrlRemoteAddress = "http://localhost:4444/wd/hub";

        public static IWebDriver GetDriver(int browser)
        {
            switch (browser)
            {
                case 1:
                    return new RemoteWebDriver(new Uri(UrlRemoteAddress), new ChromeOptions().ToCapabilities());

                default:
                    return new RemoteWebDriver(new Uri(UrlRemoteAddress), new FirefoxOptions().ToCapabilities());
            }
        }
    }
}